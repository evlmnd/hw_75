const initialState = {
    encode: '',
    decode: '',
    password: ''
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'INPUT_CHANGE':
            return {...state, [action.data.name]: action.data.value};
        case 'ENCODE_MESSAGE_SUCCESS':
            return {...state, encode: action.encoded, password: ''};
        case 'DECODE_MESSAGE_SUCCESS':
            return {...state, decode: action.decoded, password: ''};
        default:
            return state;
    }
};

export default reducer;

