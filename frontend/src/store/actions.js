import axios from '../axios-api';

export const encodeMessageSuccess = encoded => ({type: 'ENCODE_MESSAGE_SUCCESS', encoded});
export const decodeMessageSuccess = decoded => ({type: 'DECODE_MESSAGE_SUCCESS', decoded});

export const encodeMessageAction = dat => {
    return dispatch => {
        return axios.post('/encode', dat).then((response) => {
            if (response.data.encoded === dat.message) {
                alert('please use stronger password');
            } else {
                dispatch(encodeMessageSuccess(response.data.encoded));
            }
        })
    }
};

export const decodeMessageAction = dat => {
    return dispatch => {
        return axios.post('/decode', dat).then((response) => {
            dispatch(decodeMessageSuccess(response.data.decoded))
        })
    }
};