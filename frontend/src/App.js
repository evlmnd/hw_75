import React, {Component, Fragment} from 'react';
import Form from "./components/Form/Form";

class App extends Component {
  render() {
    return (
      <Fragment>
        <Form/>
      </Fragment>
    );
  }
}

export default App;
