import React, {Component} from 'react';
import './Form.css';
import {connect} from "react-redux";
import {decodeMessageAction, encodeMessageAction} from "../../store/actions";

class Form extends Component {

    inputChangeHandler = event => {
        event.preventDefault();
        this.props.inputChange({name: event.target.name, value: event.target.value});
    };

    onDecode = () => {
        if (this.props.password !== '') {
            this.props.decodeMessage({password: this.props.password, message: this.props.encode});
        } else {
            alert('enter password please');
        }
    };

    onEncode = () => {
        if (this.props.password !== '') {
            this.props.encodeMessage({password: this.props.password, message: this.props.decode});
        } else {
            alert('enter password please');
        }
    };

    render() {
        return (
            <div className="Form">
                <textarea name="decode" id="" cols="30" rows="10"
                          value={this.props.decode}
                          onChange={this.inputChangeHandler}
                          placeholder="DECODED MESSAGE"
                          style={{"margin": "20px 0"}}
                />
                <button onClick={this.onDecode}>&#8593; DECODE &#8593;</button>
                <input type="text"
                       name="password"
                       value={this.props.password}
                       placeholder="ENTER LONG PASSWORD CONSISTING DIFFERENT SYMBOLS"
                       onChange={this.inputChangeHandler}
                       style={{"margin": "20px 0"}}
                />
                <button onClick={this.onEncode}>&#8595; ENCODE &#8595;</button>
                <textarea name="encode" id="" cols="30" rows="10"
                          onChange={this.inputChangeHandler}
                          value={this.props.encode}
                          placeholder="ENCODED MESSAGE"
                          style={{"margin": "20px 0"}}
                />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        password: state.password,
        encode: state.encode,
        decode: state.decode
    }
};

const mapDispatchToProps = dispatch => ({
    encodeMessage: data => dispatch(encodeMessageAction(data)),
    decodeMessage: data => dispatch(decodeMessageAction(data)),
    inputChange: data => dispatch({type: 'INPUT_CHANGE', data})

});

export default connect(mapStateToProps, mapDispatchToProps)(Form);