const express = require('express');

const Caesar = require('caesar-salad').Caesar;
const cipher = express.Router();


cipher.post('/encode', (req, res) => {
    res.send({encoded: Caesar.Cipher(req.body.password).crypt(req.body.message)});
});

cipher.post('/decode', (req, res) => {
    res.send({decoded: Caesar.Decipher(req.body.password).crypt(req.body.message)});
});

module.exports = cipher;

