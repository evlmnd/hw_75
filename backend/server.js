const express = require('express');
const cipher = require('./app/cipher');
const cors = require('cors');
const app = express();

app.use(express.json());
app.use(cors());
app.use(cipher);

const port = 8000;

app.listen(port, () => {
    console.log(`Server starts on ${port} port`);
});
